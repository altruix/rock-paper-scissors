/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Implementation of the player interface.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class DefaultPlayer implements Player {
    /**
     * ID of the player.
     */
    private final transient String playerid;

    /**
     * Strategy employed by the player.
     */
    private final transient Strategy strategy;

    /**
     * Constructor.
     * @param pid Player ID.
     * @param pstrategy Player's strategy.
     */
    public DefaultPlayer(final String pid, final Strategy pstrategy) {
        this.playerid = pid;
        this.strategy = pstrategy;
    }

    /**
     * Returns the ID of the player.
     * @return ID of the player.
     */
    @Override
    public final String getId() {
        return this.playerid;
    }

    /**
     * Returns the strategy used by the player.
     * @return Strategy used by the player.
     */
    @Override
    public final Strategy getStrategy() {
        return this.strategy;
    }
}
