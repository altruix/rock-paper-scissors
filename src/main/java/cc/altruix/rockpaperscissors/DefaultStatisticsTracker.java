/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.lang3.StringUtils;

/**
 * Instances of this class keep track, how many games the individual players
 *  won.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.UseConcurrentHashMap")
public class DefaultStatisticsTracker implements StatisticsTracker {
    /**
     * Tie.
     */
    public static final String TIE = "Tie";

    /**
     * Map with statistics about games won by different players. Key is the
     *  player ID, value - number of games won.
     */
    private final transient Map<String, AtomicInteger> stats =
        new HashMap<>();

    /**
     * Constructor.
     */
    public DefaultStatisticsTracker() {
        this.stats.put(TIE, new AtomicInteger(0));
    }

    /**
     * Saves the data of a game round.
     * @param winner ID of the winning player, or null, if it's a tie.
     */
    @Override
    public final void saveRoundResults(final String winner) {
        final AtomicInteger counter = this.findCounter(winner);
        counter.incrementAndGet();
    }

    /**
     * Composes the text message, which summarizes the results of a series of
     *  game rounds.
     * @return Text message, which summarizes the results of a series of
     *  game rounds (number of games won by each player, number of ties).
     */
    @Override
    public final String composeResultsMessage() {
        final StringBuilder builder = new StringBuilder();
        final int total = this.calculateNumberOfGames(this.stats.values());
        final List<Map.Entry<String, AtomicInteger>> entries =
            new ArrayList<>(this.stats.entrySet());
        Collections.sort(entries, new StatsEntriesComparator());
        for (final Map.Entry<String, AtomicInteger> entry : entries) {
            if (TIE.equals(entry.getKey())) {
                continue;
            }
            builder.append(
                String.format(
                    "'%s' wins %d of %d games",
                    entry.getKey(),
                    entry.getValue().intValue(),
                    total
                )
            ).append(System.lineSeparator());
        }
        final AtomicInteger ties = this.stats.get(TIE);
        builder.append(
            String.format(
                "Tie: %d of %d games",
                ties.intValue(),
                total
            )
        ).append(System.lineSeparator());
        return builder.toString();
    }

    /**
     * Calculates total number of games.
     * @param gcounts Collection with the number of games won by each player
     *  (tie is also sort of player).
     * @return Total number of games.
     */
    private int calculateNumberOfGames(
        final Collection<AtomicInteger> gcounts) {
        return gcounts.stream().mapToInt(AtomicInteger::intValue).sum();
    }

    /**
     * Finds or creates a won games counter for the specified player.
     * @param player Player.
     * @return Counter for keeping track, of how many games that player won.
     */
    private AtomicInteger findCounter(
        final String player) {
        AtomicInteger result;
        if (StringUtils.isBlank(player)) {
            result = this.stats.get(TIE);
        } else {
            AtomicInteger counter = this.stats.get(player);
            if (counter == null) {
                counter = new AtomicInteger(0);
                this.stats.put(player, counter);
            }
            result = counter;
        }
        return result;
    }
}
