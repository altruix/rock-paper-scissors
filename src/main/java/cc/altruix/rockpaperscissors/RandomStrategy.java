/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Game strategy, in which the player randomly chooses a move.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class RandomStrategy implements Strategy {
    /**
     * Random number generator.
     */
    private final transient RandomNumberGenerator rgen;
    /**
     * Object, which maps random numbers to moves.
     */
    private final transient RandomNumberToMoveMapper mapper;

    /**
     * Constructor.
     * @param gen Random number generator.
     * @param mpr Object, which maps random numbers to moves.
     */
    public RandomStrategy(final RandomNumberGenerator gen,
        final RandomNumberToMoveMapper mpr) {
        this.rgen = gen;
        this.mapper = mpr;
    }
    /**
     * Constructor.
     * @param gen Random number generator.
     */
    public RandomStrategy(final RandomNumberGenerator gen) {
        this(gen, new DefaultRandomNumberToMoveMapper());
    }
    /**
     * Constructor.
     */
    public RandomStrategy() {
        this(new DefaultRandomNumberGenerator(),
            new DefaultRandomNumberToMoveMapper());
    }

    /**
     * Returns the next move made by the player.
     * @return Next move made by the player.
     */
    @Override
    public final Move makeMove() {
        final double experiment = this.rgen.random();
        return this.mapper.map(experiment);
    }
}
