/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class encapsulates the logic of the application.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.UseConcurrentHashMap")
public class AppLogic {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(
        AppLogic.class
    );

    /**
     * Random number generator.
     */
    private final transient RandomNumberGenerator rnumgen;
    /**
     * Object, which decides, which player won in a particular round.
     */
    private final transient WinnerPicker wpicker;
    /**
     * Game statistics tracker.
     */
    private final transient StatisticsTracker stattracker;

    /**
     * Default constructor.
     */
    public AppLogic() {
        this(new DefaultRandomNumberGenerator(),
            new DefaultWinnerPicker(),
            new DefaultStatisticsTracker());
    }

    /**
     * Constructor.
     * @param rgen Random number generator.
     * @param wpkr Winner picker object.
     * @param statstracker Statistics tracking object.
     */
    public AppLogic(final RandomNumberGenerator rgen,
        final WinnerPicker wpkr,
        final StatisticsTracker statstracker) {
        this.rnumgen = rgen;
        this.wpicker = wpkr;
        this.stattracker = statstracker;
    }

    /**
     * Runs the application.
     * @param runs Number of game rounds.
     */
    public final void run(final int runs) {
        final Player[] players = this.createPlayers();
        for (int round = 0; round < runs; ++round) {
            this.gameRound(players);
        }
        LOGGER.info(this.stattracker.composeResultsMessage());
    }

    /**
     * Creates the players.
     * @return Array with 2 players.
     */
    protected final Player[] createPlayers() {
        return new Player[] {
            new DefaultPlayer(
                "Player A",
                new FixedOutcomeStrategy(Move.PAPER)
            ),
            new DefaultPlayer(
                "Player B",
                new RandomStrategy(this.rnumgen)
            ),
        };
    }

    /**
     * One game round - the players do their moves, then we pick a winner and
     *  save the information about it in the tracker.
     * @param players Players' array.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    protected void gameRound(final Player[] players) {
        final Map<String, Move> movesByPlayer = this.makeMoves(players);
        final String winner = this.wpicker.pickWinner(
            this.createEntryList(movesByPlayer)
        );
        this.stattracker.saveRoundResults(winner);
    }

    /**
     * Converts a map into a list of entries. We need this method for easier
     *  testing/mocking.
     * @param moves Map to convert.
     * @return List of key-value entries of the map.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    protected List<Map.Entry<String, Move>> createEntryList(
        final Map<String, Move> moves) {
        return new ArrayList<>(moves.entrySet());
    }

    /**
     * Makes all players make their moves.
     * @param players Players' array.
     * @return Map with players' IDs as keys and their moves as values.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    protected Map<String, Move> makeMoves(final Player[] players) {
        final Map<String, Move> moves = new HashMap<>();
        boolean valid = true;
        if (players == null) {
            LOGGER.error("players array is null");
            valid = false;
        }
        if (valid && (players.length != 2)) {
            LOGGER.error("Number of players array is invalid");
            valid = false;
        }
        if (valid) {
            for (final Player player : players) {
                this.letPlayMakeMove(player, moves);
            }
        }
        return moves;
    }

    /**
     * Represents a single move of a particular player in a round.
     * @param player Player, who makes the move.
     * @param moves Map with moves organized by player IDs as keys.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    protected void letPlayMakeMove(final Player player,
        final Map<String, Move> moves) {
        if (player == null) {
            LOGGER.error("Player is null");
            return;
        }
        boolean valid = true;
        final String plid = player.getId();
        if (valid && StringUtils.isBlank(plid)) {
            LOGGER.error("Player has no ID");
            valid = false;
        }
        final Strategy strategy = player.getStrategy();
        if (valid && (strategy == null)) {
            LOGGER.error(String.format("Player '%s' has no strategy", plid));
            valid = false;
        }
        if (valid) {
            final Move move = strategy.makeMove();
            if (move == null) {
                LOGGER.error(
                    String.format(
                        "Strategy of player '%s' returned null move", plid
                    )
                );
                valid = false;
            }
            if (valid) {
                moves.put(plid, move);
            }
        }
    }
}
