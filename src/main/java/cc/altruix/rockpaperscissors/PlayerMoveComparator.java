/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Instance of this class determine, which of two moves is stronger (here the
 *  logic of "X beats Y" is encapsulated).
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.MissingSerialVersionUID")
public class PlayerMoveComparator
    implements Comparator<Map.Entry<String, Move>>, Serializable {
    /**
     * Return value of the compare method, when the first move is stronger
     *  than the second.
     */
    public static final Integer FIRST_STRONGER = Integer.valueOf(-1);
    /**
     * Return value of the compare method, when the second move is stronger
     *  than the first.
     */
    public static final Integer SECOND_STRONGER = Integer.valueOf(1);
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(
        PlayerMoveComparator.class
    );
    /**
     * This MultiKeyMap contains data about the strengths of individual moves.
     *  Each entry has two keys (first move, second move) and the value is an
     *  int, which tells, which of them is stronger.
     */
    private final transient MultiKeyMap<Move, Integer> movestrengths;

    /**
     * Constructor.
     */
    public PlayerMoveComparator() {
        this.movestrengths = new MultiKeyMap<>();
        this.movestrengths.put(
            Move.ROCK,
            Move.PAPER,
            FIRST_STRONGER
        );
        this.movestrengths.put(
            Move.ROCK,
            Move.SCISSORS,
            SECOND_STRONGER
        );
        this.movestrengths.put(
            Move.PAPER,
            Move.ROCK,
            SECOND_STRONGER
        );
        this.movestrengths.put(
            Move.PAPER,
            Move.SCISSORS,
            FIRST_STRONGER
        );
        this.movestrengths.put(
            Move.SCISSORS,
            Move.ROCK,
            FIRST_STRONGER
        );
        this.movestrengths.put(
            Move.SCISSORS,
            Move.PAPER,
            SECOND_STRONGER
        );
    }

    /**
     * Compares two entries with moves and determines, which entry contains a
     *  stronger move.
     * @param first First entry.
     * @param second Second entry.
     * @return Zero, -1 or 1, depending on which entry has a stronger move.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    @Override
    public int compare(final Map.Entry<String, Move> first,
        final Map.Entry<String, Move> second) {
        boolean valid = true;
        int result;
        if ((first == null) || (second == null)) {
            LOGGER.error("Invalid operation - one of the operands is null");
            valid = false;
        }
        if (valid) {
            final Move fmove = first.getValue();
            final Move smove = second.getValue();
            if ((fmove == null) || (smove == null)) {
                LOGGER.error("Invalid operation - at least one move is null");
                result = 0;
            } else {
                result = this.compareMoves(fmove, smove);
            }
        } else  {
            result = 0;
        }
        return result;
    }

    /**
     * Determines, which move is stronger.
     * @param first First move.
     * @param second Second move.
     * @return Zero, -1 or 1, depending on which entry has a stronger move.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    protected int compareMoves(final Move first, final Move second) {
        int result;
        if ((first == null) || (second == null)) {
            LOGGER.error("One of the moves is null");
            result = 0;
        } else if (first.equals(second)) {
            result = 0;
        } else {
            result = this.movestrengths.get(first, second);
        }
        return result;
    }
}
