/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * A game strategy, in which the player always does the same move.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class FixedOutcomeStrategy implements Strategy {
    /**
     * Move, which the player does in every round.
     */
    private final transient Move outcome;

    /**
     * Constructor.
     * @param move Move, which the player will do in every round.
     */
    public FixedOutcomeStrategy(final Move move) {
        this.outcome = move;
    }

    /**
     * Determines the move, which the player will make in this round.
     * @return The move, which the player will make in this round.
     */
    @Override
    public final Move makeMove() {
        return this.outcome;
    }
}
