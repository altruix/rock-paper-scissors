/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Classes implementing this interfaces keep track, how many games the
 *  individual players won.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface StatisticsTracker {
    /**
     * Saves the result of a game round.
     * @param winner ID of the winning player, or null, if it's a tie.
     */
    void saveRoundResults(final String winner);

    /**
     * Composes the text message, which summarizes the results of a series of
     *  game rounds.
     * @return Text message, which summarizes the results of a series of
     *  game rounds (number of games won by each player, number of ties).
     */
    String composeResultsMessage();
}
