/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Interfaces for random number generators.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface RandomNumberGenerator {
    /**
     * Generates a random number from 0 to 1.
     * @return Random number from zero to 1.
     */
    double random();
}
