/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.util.List;
import java.util.Map;

/**
 * Objects, which implement this interface determine, which of the players won
 *  a round.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface WinnerPicker {
    /**
     * Determines, who of the players won a particular game round.
     * @param entries Key-value entries, key is the name of the player, value -
     *  the move (rock, paper, scissors) he made during the last game round.
     * @return ID of the winning player.
     */
    String pickWinner(final List<Map.Entry<String, Move>> entries);
}
