/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This object determines, which of the players won a round.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class DefaultWinnerPicker implements WinnerPicker {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(
        DefaultWinnerPicker.class
    );
    /**
     * Object for comparing the results of each player.
     */
    private final transient Comparator<Map.Entry<String, Move>> mcomparator;

    /**
     * Constructor.
     * @param comp Object for comparing the results of each player.
     */
    public DefaultWinnerPicker(final Comparator<Map.Entry<String, Move>> comp) {
        this.mcomparator = comp;
    }

    /**
     * Constructor.
     */
    public DefaultWinnerPicker() {
        this(new PlayerMoveComparator());
    }

    /**
     * Determines, who of the players won a particular game round.
     * @param entries Key-value entries, key is the name of the player, value -
     *  the move (rock, paper, scissors) he made during the last game round.
     * @return ID of the winning player.
     * @checkstyle DesignForExtensionCheck (3 lines)
     */
    @Override
    public String pickWinner(final List<Map.Entry<String, Move>> entries) {
        boolean valid = true;
        if (entries == null) {
            LOGGER.error("movesByPlayers is null");
            valid = false;
        } else if (entries.size() != 2) {
            LOGGER.error("Currently, only 2-player games are supported");
            valid = false;
        }
        final String result;
        if (valid) {
            final int compres = this.mcomparator.compare(
                entries.get(0),
                entries.get(1)
            );
            if (compres == 0) {
                result = null;
            } else if (compres < 0) {
                result = entries.get(0).getKey();
            } else {
                result = entries.get(1).getKey();
            }
        } else {
            result = null;
        }
        return result;
    }
}
