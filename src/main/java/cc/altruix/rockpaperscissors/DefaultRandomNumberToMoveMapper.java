/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Instances of this class are used to create a move based on a random number.
 *  All moves are evenly distributed (all three have equal probability of
 *  occuring as a result of the map method).
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class DefaultRandomNumberToMoveMapper
    implements RandomNumberToMoveMapper {
    /**
     * Lower bound of the 0.-1. continuum, which corresponds to rock.
     */
    public static final int ROCK_LBOUND = 0;
    /**
     * Number of possible moves.
     */
    public static final double NUMBER_OF_MOVES = 3.;
    /**
     * Upper bound of the 0.-1. continuum, which corresponds to rock.
     */
    public static final double ROCK_UBOUND = 1. / NUMBER_OF_MOVES;
    /**
     * Lower bound of the 0.-1. continuum, which corresponds to paper.
     */
    public static final double PAPER_LBOUND = 1. / NUMBER_OF_MOVES;
    /**
     * Upper bound of the 0.-1. continuum, which corresponds to paper.
     */
    public static final double PAPER_UBOUND = 2. / NUMBER_OF_MOVES;

    /**
     * Maps a random number to a move (rock, paper, scissors).
     * @param rnum Random number.
     * @return Rock, paper or scissors, depending on the value of the random
     *  number.
     */
    @Override
    public final Move map(final double rnum) {
        final Move result;
        if ((rnum >= ROCK_LBOUND) && (rnum < ROCK_UBOUND)) {
            result = Move.ROCK;
        } else if ((rnum >= PAPER_LBOUND) && (rnum < PAPER_UBOUND)) {
            result = Move.PAPER;
        } else {
            result = Move.SCISSORS;
        }
        return result;
    }
}
