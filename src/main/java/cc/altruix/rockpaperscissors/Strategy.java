/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Objects implementing this interface determine, how a player
 *  makes a decision about which move to make.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface Strategy {
    /**
     * Returns the next move made by the player.
     * @return Next move made by the player.
     */
    Move makeMove();
}
