/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Represents moves, which a player can make during a game round.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public enum Move {
  /**
   * Rock.
   */
  ROCK,
  /**
   * Paper.
   */
  PAPER,
  /**
   * Scissors.
   */
  SCISSORS
}
