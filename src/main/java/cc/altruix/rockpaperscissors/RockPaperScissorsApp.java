/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Main class of the application.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class RockPaperScissorsApp {
    /**
     * Number of game rounds.
     */
    public static final int RUNS_COUNT = 100;

    /**
     * Constructor.
     */
    private RockPaperScissorsApp() {
    }
    /**
     * Main method of the application.
     * @param args Command-line arguments.
     */
    public static void main(final String[] args) {
        final AppLogic logic = new AppLogic();
        logic.run(RUNS_COUNT);
    }
}
