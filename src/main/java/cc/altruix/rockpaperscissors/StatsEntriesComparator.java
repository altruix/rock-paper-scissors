/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Compares two entries with data about player and his move, and tells, which
 *  of them contains a stronger move.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings("PMD.MissingSerialVersionUID")
public class StatsEntriesComparator
    implements Comparator<Map.Entry<String, AtomicInteger>>, Serializable {
    /**
     * Logger.
     */
    private static final Logger LOGGER =
        LoggerFactory.getLogger(
            StatsEntriesComparator.class
        );

    /**
     * Compares two entries and determines, which of them has a stronger move.
     * @param first First entry.
     * @param second Second entry.
     * @return Zero, -1 or 1, depending, on which entry has the stronger move.
     */
    @Override
    public final int compare(final Map.Entry<String, AtomicInteger> first,
        final Map.Entry<String, AtomicInteger> second) {
        int result;
        if ((first == null) || (second == null)) {
            LOGGER.debug("At least one stats entry is null");
            result = 0;
        } else if ((first.getKey() == null) || (second.getKey() == null)) {
            LOGGER.debug("At least one player's name is null");
            result = 0;
        } else if (DefaultStatisticsTracker.TIE.equals(first.getKey())) {
            result = -1;
        } else if (DefaultStatisticsTracker.TIE.equals(second.getKey())) {
            result = 1;
        } else {
            result = first.getKey().compareTo(second.getKey());
        }
        return result;
    }
}
