/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Classes implementing this interface are used to create a move based on a
 *  random number.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface RandomNumberToMoveMapper {
    /**
     * Maps a random number to a move (rock, paper, scissors).
     * @param rnum Random number.
     * @return Rock, paper or scissors, depending on the value of the random
     *  number.
     */
    Move map(final double rnum);
}
