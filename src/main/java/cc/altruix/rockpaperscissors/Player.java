/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Represents a player.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface Player {
    /**
     * Returns the ID of the player.
     * @return ID of the player.
     */
    String getId();

    /**
     * Returns the strategy used by the player.
     * @return Strategy used by the player.
     */
    Strategy getStrategy();
}
