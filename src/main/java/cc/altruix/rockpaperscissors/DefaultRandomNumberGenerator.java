/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

/**
 * Default (Math.random) random number generator.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class DefaultRandomNumberGenerator implements RandomNumberGenerator {
    /**
     * Returns a random number between 0 and 1.
     * @return Random number between 0 and 1.
     */
    @Override
    public final double random() {
        return Math.random();
    }
}
