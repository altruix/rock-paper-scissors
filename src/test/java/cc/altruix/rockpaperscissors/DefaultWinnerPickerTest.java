/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

/**
 * Unit tests for DefaultWinnerPicker.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class DefaultWinnerPickerTest {
    /**
     * Player 1.
     */
    public static final String PLAYER_1 = "Player 1";
    /**
     * Player 2.
     */
    public static final String PLAYER_2 = "Player 2";
    /**
     * Three players.
     */
    public static final int TRHEE_PLAYERS = 3;

    /**
     * Unit test.
     */
    @Test
    public void pickWinnerDoesntCrashOnNullInputMap() {
        final DefaultWinnerPicker testObject = new DefaultWinnerPicker(null);
        Assertions.assertThat(testObject.pickWinner(null)).isNull();
    }
    /**
     * Unit test.
     */
    @Test
    public void pickWinnerReturnsNullOnWronglySizedInputMaps() {
        final DefaultWinnerPicker testObject = new DefaultWinnerPicker(null);
        Assertions.assertThat(
            testObject.pickWinner(
                this.createMap(0)
            )
        ).isNull();
        Assertions.assertThat(
            testObject.pickWinner(
                this.createMap(1)
            )
        ).isNull();
        Assertions.assertThat(
            testObject.pickWinner(
                this.createMap(TRHEE_PLAYERS)
            )
        ).isNull();
    }
    /**
     * Unit test.
     */
    @Test
    public void pickWinnerReturnsNullOnEqualMoves() {
        final List<Map.Entry<String, Move>> input = this.createTestData();
        final Comparator<Map.Entry<String, Move>> comp =
            this.createComparatorMock(0);
        final DefaultWinnerPicker testObject = new DefaultWinnerPicker(comp);
        Assertions.assertThat(testObject.pickWinner(input)).isNull();
    }
    /**
     * Unit test.
     */
    @Test
    public void pickWinnerReturnsFirstPlayerIfNecessary() {
        final List<Map.Entry<String, Move>> input = this.createTestData();
        final Comparator<Map.Entry<String, Move>> comp =
            this.createComparatorMock(-1);
        final DefaultWinnerPicker testObject = new DefaultWinnerPicker(comp);
        Assertions.assertThat(testObject.pickWinner(input)).isEqualTo(PLAYER_1);
    }
    /**
     * Unit test.
     */
    @Test
    public void pickWinnerReturnsSecondPlayerIfNecessary() {
        final List<Map.Entry<String, Move>> input = this.createTestData();
        final Comparator<Map.Entry<String, Move>> comp =
            this.createComparatorMock(1);
        final DefaultWinnerPicker testObject = new DefaultWinnerPicker(comp);
        Assertions.assertThat(testObject.pickWinner(input)).isEqualTo(PLAYER_2);
    }
    /**
     * Creates test data.
     * @return Test data.
     */
    private List<Map.Entry<String, Move>> createTestData() {
        final List<Map.Entry<String, Move>> input = new ArrayList<>(2);
        input.add(new AbstractMap.SimpleEntry<>(PLAYER_1, Move.ROCK));
        input.add(new AbstractMap.SimpleEntry<>(PLAYER_2, Move.ROCK));
        return input;
    }
    /**
     * Creates a comparator mock.
     * @param rval Random number.
     * @return Comparator mock.
     */
    private Comparator<Map.Entry<String, Move>> createComparatorMock(
        final int rval) {
        final Comparator<Map.Entry<String, Move>> comp =
            Mockito.mock(Comparator.class);
        Mockito.when(comp.compare(Matchers.anyObject(), Matchers.anyObject()))
            .thenReturn(Integer.valueOf(rval));
        return comp;
    }
    /**
     * Creates test data.
     * @param entcount Number of entrie.
     * @return Test data.
     */
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private List<Map.Entry<String, Move>> createMap(final int entcount) {
        final List<Map.Entry<String, Move>> list = new ArrayList<>(entcount);
        for (int player = 0; player < entcount; ++player) {
            list.add(
                new AbstractMap.SimpleEntry<>(
                    String.format(
                        "Player %d",
                        player
                    ),
                    Move.ROCK
                )
            );
        }
        return list;
    }
}
