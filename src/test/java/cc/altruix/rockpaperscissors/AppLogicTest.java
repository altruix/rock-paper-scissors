/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

/**
 * Tests for the AppLogic class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
@SuppressWarnings({ "PMD.AppLogicTest", "PMD.UseConcurrentHashMap",
    "PMD.TooManyMethods" })
public final class AppLogicTest {
    /**
     * Player name.
     */
    public static final String PLAYER_1 = "Player 1";
    /**
     * Number of game runs.
     */
    public static final int RUNS = 100;

    /**
     * Three players.
     */
    public static final int THREE_PLAYERS = 3;

    /**
     * Unit test.
     */
    @Test
    public void letPlayMakeMoveAddsPlayersMove() {
        final Strategy strategy = Mockito.mock(Strategy.class);
        Mockito.when(strategy.makeMove()).thenReturn(Move.SCISSORS);
        final Player player = Mockito.mock(Player.class);
        Mockito.when(player.getId()).thenReturn(PLAYER_1);
        Mockito.when(player.getStrategy()).thenReturn(strategy);
        final AppLogic appLogic = new AppLogic();
        final Map<String, Move> moves = new HashMap<>();
        appLogic.letPlayMakeMove(player, moves);
        Assertions.assertThat(moves).isNotEmpty();
        Assertions.assertThat(moves.get(PLAYER_1)).isEqualTo(Move.SCISSORS);
    }

    /**
     * Unit test.
     */
    @Test
    public void letPlayMakeMoveDoesNotAddMoveIfPlayerIsNull() {
        final AppLogic appLogic = new AppLogic();
        final Map<String, Move> moves = new HashMap<>();
        appLogic.letPlayMakeMove(null, moves);
        Assertions.assertThat(moves).isEmpty();
    }

    /**
     * Unit test.
     */
    @Test
    public void letPlayMakeMoveDoesNotAddMoveIfPlayerHasNoId() {
        final Strategy strategy = Mockito.mock(Strategy.class);
        Mockito.when(strategy.makeMove()).thenReturn(Move.SCISSORS);
        final Player player = Mockito.mock(Player.class);
        Mockito.when(player.getId()).thenReturn(null);
        final AppLogic appLogic = new AppLogic();
        final Map<String, Move> moves = new HashMap<>();
        appLogic.letPlayMakeMove(player, moves);
        Assertions.assertThat(moves).isEmpty();
    }

    /**
     * Unit test.
     */
    @Test
    public void letPlayMakeMoveDoesNotAddMoveIfPlayerHasNoStrategy() {
        final Player player = Mockito.mock(Player.class);
        Mockito.when(player.getId()).thenReturn(PLAYER_1);
        Mockito.when(player.getStrategy()).thenReturn(null);
        final AppLogic appLogic = new AppLogic();
        final Map<String, Move> moves = new HashMap<>();
        appLogic.letPlayMakeMove(player, moves);
        Assertions.assertThat(moves).isEmpty();
    }

    /**
     * Unit test.
     */
    @Test
    public void letPlayMakeMoveDoesNotAddMoveIfStrategyReturnsNullMove() {
        final Strategy strategy = Mockito.mock(Strategy.class);
        Mockito.when(strategy.makeMove()).thenReturn(null);
        final Player player = Mockito.mock(Player.class);
        Mockito.when(player.getId()).thenReturn(PLAYER_1);
        Mockito.when(player.getStrategy()).thenReturn(strategy);
        final AppLogic appLogic = new AppLogic();
        final Map<String, Move> moves = new HashMap<>();
        appLogic.letPlayMakeMove(player, moves);
        Assertions.assertThat(moves).isEmpty();
    }

    /**
     * Unit test.
     */
    @Test
    public void makeMovesDoesNothingIfInputIsNull() {
        final AppLogic appLogic = Mockito.spy(new AppLogic());
        appLogic.makeMoves(null);
        Mockito.verify(appLogic, Mockito.never()).letPlayMakeMove(
            Matchers.anyObject(),
            Matchers.anyObject()
        );
    }

    /**
     * Unit test.
     */
    @Test
    public void makeMovesDoesNothingIfNumberOfPlayersWrong() {
        this.makeMovesNumberOfPlayersTestLogic(0);
        this.makeMovesNumberOfPlayersTestLogic(1);
        this.makeMovesNumberOfPlayersTestLogic(THREE_PLAYERS);
    }

    /**
     * Unit test.
     */
    @Test
    public void makeMovesCallsLetPlayMakeMoveForAllPlayers() {
        final Player first = Mockito.mock(Player.class);
        final Player second = Mockito.mock(Player.class);
        final AppLogic appLogic = Mockito.spy(new AppLogic());
        appLogic.makeMoves(new Player[]{first, second});
        Mockito.verify(appLogic, Mockito.times(1)).letPlayMakeMove(
            Matchers.same(first),
            Matchers.any(Map.class)
        );
        Mockito.verify(appLogic, Mockito.times(1)).letPlayMakeMove(
            Matchers.same(second),
            Matchers.any(Map.class)
        );
    }

    /**
     * Unit test.
     */
    @Test
    public void createPlayersCreatesValidPlayers() {
        final AppLogic logic = Mockito.spy(new AppLogic());
        final Player[] players = logic.createPlayers();
        Assertions.assertThat(players).isNotNull();
        Assertions.assertThat(players.length).isEqualTo(2);
        for (final Player player : players) {
            Assertions.assertThat(player).isNotNull();
            Assertions.assertThat(player.getId()).isNotEmpty();
            Assertions.assertThat(player.getStrategy()).isNotNull();
        }
    }

    /**
     * Unit test.
     */
    @Test
    public void gameRoundIsWiredProperly() {
        final WinnerPicker wpicker = Mockito.mock(
            WinnerPicker.class
        );
        final StatisticsTracker statstracker = Mockito.mock(
            StatisticsTracker.class
        );
        final Player[] players = new Player[0];
        final Map<String, Move> movesByPlayer = Mockito.mock(Map.class);
        final List<Map.Entry<String, Move>> entriesList = Mockito.mock(
            List.class
        );
        final String winner = "Winner";
        Mockito.when(wpicker.pickWinner(entriesList)).thenReturn(winner);
        final AppLogic logic = Mockito.spy(
            new AppLogic(
                Mockito.mock(RandomNumberGenerator.class),
                wpicker,
                statstracker
            )
        );
        Mockito.doReturn(entriesList).when(logic)
            .createEntryList(movesByPlayer);
        Mockito.doReturn(movesByPlayer).when(logic).makeMoves(players);
        logic.gameRound(players);
        Mockito.verify(statstracker, Mockito.times(1)).saveRoundResults(winner);
        Mockito.verify(wpicker, Mockito.times(1)).pickWinner(entriesList);
        Mockito.verify(logic, Mockito.times(1)).createEntryList(movesByPlayer);
        Mockito.verify(logic, Mockito.times(1)).makeMoves(players);
    }

    /**
     * Unit test.
     */
    @Test
    public void runCallsGameRoundAsOftenAsNecessary() {
        final StatisticsTracker statstracker = Mockito.mock(
            StatisticsTracker.class
        );
        final AppLogic logic = Mockito.spy(
            new AppLogic(
                Mockito.mock(RandomNumberGenerator.class),
                Mockito.mock(WinnerPicker.class),
                statstracker
            )
        );
        Mockito.doNothing().when(logic).gameRound(Matchers.any());
        logic.run(RUNS);
        Mockito.verify(logic, Mockito.times(RUNS)).gameRound(Matchers.any());
        Mockito.verify(statstracker, Mockito.times(1)).composeResultsMessage();
    }

    /**
     * Logic of testing the makeMoves method.
     * @param players Number of players.
     */
    private void makeMovesNumberOfPlayersTestLogic(final int players) {
        final AppLogic appLogic = Mockito.spy(new AppLogic());
        appLogic.makeMoves(new Player[players]);
        Mockito.verify(appLogic, Mockito.never()).letPlayMakeMove(
            Matchers.anyObject(),
            Matchers.anyObject()
        );
    }
}
