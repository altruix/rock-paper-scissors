/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Unit tests for the RandomStrategy class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class RandomStrategyTest {
    /**
     * Unit test.
     */
    @Test
    public void makeMoveCorrectlyMapsRandomNumbersToMoves() {
        this.makeMoveTestLogic(
            DefaultRandomNumberToMoveMapper.ROCK_LBOUND,
            DefaultRandomNumberToMoveMapper.ROCK_LBOUND
                + DefaultRandomNumberToMoveMapperTest.ONE_MILLIONTH,
            DefaultRandomNumberToMoveMapper.ROCK_UBOUND
                - DefaultRandomNumberToMoveMapperTest.ONE_MILLIONTH,
            Move.ROCK
        );
        this.makeMoveTestLogic(
            DefaultRandomNumberToMoveMapper.PAPER_LBOUND,
            DefaultRandomNumberToMoveMapper.PAPER_LBOUND
                + DefaultRandomNumberToMoveMapperTest.ONE_MILLIONTH,
            DefaultRandomNumberToMoveMapper.PAPER_UBOUND
                - DefaultRandomNumberToMoveMapperTest.ONE_MILLIONTH,
            Move.PAPER
        );
        this.makeMoveTestLogic(
            DefaultRandomNumberToMoveMapper.PAPER_UBOUND,
            DefaultRandomNumberToMoveMapper.PAPER_UBOUND
                + DefaultRandomNumberToMoveMapperTest.ONE_MILLIONTH,
            1. - DefaultRandomNumberToMoveMapperTest.ONE_MILLIONTH,
            Move.SCISSORS
        );
    }

    /**
     * Logic for testing the makeMove method.
     * @param first Random number, which the random number generator returns
     *  upon first call.
     * @param second Random number, which the random number generator returns
     *  upon second call.
     * @param third Random number, which the random number generator returns
     *  upon third call.
     * @param move Expected move.
     * @checkstyle ParameterNumberCheck (3 lines)
     */
    private void makeMoveTestLogic(final double first, final double second,
        final double third, final Move move) {
        final RandomNumberGenerator rgen = Mockito.mock(
            RandomNumberGenerator.class
        );
        Mockito.when(rgen.random()).thenReturn(first);
        Mockito.when(rgen.random()).thenReturn(second);
        Mockito.when(rgen.random()).thenReturn(third);
        final RandomStrategy strategy = new RandomStrategy(rgen);
        Assertions.assertThat(strategy.makeMove()).isEqualTo(move);
    }
}
