/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import org.fest.assertions.Assertions;
import org.junit.Test;

/**
 * Unit tests for FixedOutcomeStrategy.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class FixedOutcomeStrategyTest {
    /**
     * Unit test.
     */
    @Test
    public void makeMoveAlwaysReturnsFixedMove() {
        final FixedOutcomeStrategy strategy =
            new FixedOutcomeStrategy(Move.PAPER);
        Assertions.assertThat(strategy.makeMove()).isEqualTo(Move.PAPER);
        Assertions.assertThat(strategy.makeMove()).isEqualTo(Move.PAPER);
    }
}
