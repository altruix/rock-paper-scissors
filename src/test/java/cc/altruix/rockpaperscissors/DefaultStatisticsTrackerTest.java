/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import org.fest.assertions.Assertions;
import org.junit.Test;

/**
 * Tests for DefaultStatisticsTracker class.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class DefaultStatisticsTrackerTest {
    /**
     * Player 1.
     */
    public static final String PLAYER_1 = "Player 1";
    /**
     * Player 2.
     */
    public static final String PLAYER_2 = "Player 2";

    /**
     * Unit test.
     */
    @Test
    public void statsCountingWorksCorrectly() {
        final DefaultStatisticsTracker tracker = new DefaultStatisticsTracker();
        tracker.saveRoundResults(null);
        tracker.saveRoundResults(PLAYER_1);
        tracker.saveRoundResults(PLAYER_1);
        tracker.saveRoundResults(PLAYER_2);
        tracker.saveRoundResults(PLAYER_2);
        tracker.saveRoundResults(PLAYER_2);
        Assertions.assertThat(tracker.composeResultsMessage()).isEqualTo(
            String.format(
                "%s%s%s%s%s%s",
                "'Player 1' wins 2 of 6 games",
                System.lineSeparator(),
                "'Player 2' wins 3 of 6 games",
                System.lineSeparator(),
                "Tie: 1 of 6 games",
                System.lineSeparator()
            )
        );
    }
}
