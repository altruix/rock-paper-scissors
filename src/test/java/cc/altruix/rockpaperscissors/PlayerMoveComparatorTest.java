/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import java.util.Map;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Unit tests for PlayerMoveComparator.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class PlayerMoveComparatorTest {
    /**
     * Comparison result.
     */
    public static final int COMPARISON_RESULT = 1106;

    /**
     * Unit test.
     */
    @Test
    public void compareMovesCorrectlyComparesMoves() {
        final PlayerMoveComparator testObject = new PlayerMoveComparator();
        Assertions.assertThat(testObject.compareMoves(Move.ROCK, Move.PAPER))
            .isLessThan(0);
        Assertions.assertThat(
            testObject.compareMoves(Move.ROCK, Move.SCISSORS)
        ).isGreaterThan(0);
        Assertions.assertThat(testObject.compareMoves(Move.PAPER, Move.ROCK))
            .isGreaterThan(0);
        Assertions.assertThat(
            testObject.compareMoves(Move.PAPER, Move.SCISSORS)
        ).isLessThan(0);
        Assertions.assertThat(testObject.compareMoves(Move.SCISSORS, Move.ROCK))
            .isLessThan(0);
        Assertions.assertThat(
            testObject.compareMoves(Move.SCISSORS, Move.PAPER)
        ).isGreaterThan(0);
        Assertions.assertThat(
            testObject.compareMoves(Move.SCISSORS, Move.SCISSORS)
        ).isEqualTo(0);
        Assertions.assertThat(testObject.compareMoves(Move.PAPER, Move.PAPER))
            .isEqualTo(0);
        Assertions.assertThat(testObject.compareMoves(Move.ROCK, Move.ROCK))
            .isEqualTo(0);
    }
    /**
     * Unit test.
     */
    @Test
    public void compareReturnsZeroOnNullInputParameters() {
        final PlayerMoveComparator testObject = new PlayerMoveComparator();
        Assertions.assertThat(
            testObject.compare(
                null,
                Mockito.mock(Map.Entry.class)
            )
        ).isEqualTo(0);
        Assertions.assertThat(
            testObject.compare(
                Mockito.mock(Map.Entry.class),
                null
            )
        ).isEqualTo(0);
    }
    /**
     * Unit test.
     */
    @Test
    public void compareReturnsZeroOnInputParametersWithNullValues() {
        final PlayerMoveComparator testObject = new PlayerMoveComparator();
        Assertions.assertThat(
            testObject.compare(
                this.mockEntry(null),
                this.mockEntry(Move.ROCK)
            )
        ).isEqualTo(0);
        Assertions.assertThat(
            testObject.compare(
                this.mockEntry(Move.ROCK),
                this.mockEntry(null)
            )
        ).isEqualTo(0);
    }
    /**
     * Unit test.
     */
    @Test
    public void compareReturnCompareMovesReturnValue() {
        final PlayerMoveComparator testObject = Mockito.spy(
            new PlayerMoveComparator()
        );
        final Move first = Move.ROCK;
        final Move second = Move.PAPER;
        final Map.Entry<String, Move> fentry = this.mockEntry(first);
        final Map.Entry<String, Move> sentry = this.mockEntry(second);
        Mockito.when(testObject.compareMoves(first, second)).thenReturn(
            COMPARISON_RESULT
        );
        testObject.compare(fentry, sentry);
        Mockito.verify(testObject, Mockito.times(1))
            .compareMoves(first, second);
    }

    /**
     * Creates a mock of an entry.
     * @param value Move, which the entry should have as its value.
     * @return Mock entry.
     */
    private Map.Entry<String, Move> mockEntry(final Move value) {
        final Map.Entry<String, Move> entry = Mockito.mock(Map.Entry.class);
        Mockito.when(entry.getValue()).thenReturn(value);
        return entry;
    }
}
