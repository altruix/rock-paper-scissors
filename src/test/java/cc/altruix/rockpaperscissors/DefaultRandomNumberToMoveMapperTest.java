/**
 * This file is part of a test programming task.
 */
package cc.altruix.rockpaperscissors;

import org.fest.assertions.Assertions;
import org.junit.Test;

/**
 * Tests for DefaultRandomNumberToMoveMapper.
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class DefaultRandomNumberToMoveMapperTest {
    /**
     * One million.
     */
    public static final double ONE_MILLION = 1000000.;
    /**
     * One-millionth.
     */
    public static final double ONE_MILLIONTH = 1. / ONE_MILLION;

    /**
     * Unit test.
     */
    @Test
    public void runWorksInRockRange() {
        final RandomNumberToMoveMapper mapper =
            new DefaultRandomNumberToMoveMapper();
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.ROCK_LBOUND
            )
        ).isEqualTo(Move.ROCK);
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.ROCK_LBOUND + ONE_MILLIONTH
            )
        ).isEqualTo(Move.ROCK);
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.ROCK_UBOUND - ONE_MILLIONTH
            )
        ).isEqualTo(Move.ROCK);
    }
    /**
     * Unit test.
     */
    @Test
    public void runWorksInPaperRange() {
        final RandomNumberToMoveMapper mapper =
            new DefaultRandomNumberToMoveMapper();
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.PAPER_LBOUND
            )
        ).isEqualTo(Move.PAPER);
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.PAPER_LBOUND + ONE_MILLIONTH
            )
        ).isEqualTo(Move.PAPER);
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.PAPER_UBOUND - ONE_MILLIONTH
            )
        ).isEqualTo(Move.PAPER);
    }
    /**
     * Unit test.
     */
    @Test
    public void runWorksInScissorsRange() {
        final RandomNumberToMoveMapper mapper =
            new DefaultRandomNumberToMoveMapper();
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.PAPER_UBOUND
            )
        ).isEqualTo(Move.SCISSORS);
        Assertions.assertThat(
            mapper.map(
                DefaultRandomNumberToMoveMapper.PAPER_UBOUND + ONE_MILLIONTH
            )
        ).isEqualTo(Move.SCISSORS);
        Assertions.assertThat(
            mapper.map(1. - ONE_MILLIONTH)
        ).isEqualTo(Move.SCISSORS);
        Assertions.assertThat(mapper.map(1.)).isEqualTo(Move.SCISSORS);
    }
}
